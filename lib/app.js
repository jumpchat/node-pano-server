var rtc = require('webrtc-node')
var morgan = require('morgan')
var uuid = require('uuid/v1')
var _ = require('lodash')
var bodyParser = require('body-parser')
var express = require('express')
var config = require('config')
var winston = require('winston')
var jcclient = require('./jcclient')
var fetch = require('fetch')
var JanusClient = require('./JanusClient')
var rtc = require('webrtc-node')

var RTCPeerConnection = rtc.RTCPeerConnection
var RTCIceCandidate = rtc.RTCIceCandidate

rtc.setDebug(true);

class App {

    constructor() {
        this.server = express()
        this.router = express.Router()
        this.state =  {
            pcs: {}
        }
    }

    parseArgs() {

    }

    init() {
        App.initLogger()
        this.initMediaStream()
        this.initExpress()
        this.initJumpChat()
        this.initJanus()

        var sources;
        var self = this;
        setInterval(function() {
            rtc.getSources(s => {
                if (sources) {
                    if (s.length < sources.length) {
                        console.log('Camera removed')
                    } else if (s.length > sources.length) {
                        console.log('Camera found.  Reseting media stream.')
                        self.resetStream();
                    }
                }
                sources = s;
            })                
        }, 1000)
    }

    initMediaStream() {
        var self = this;
        var constraints = {
            video: config.get('webrtc.camera') != null,
            audio: config.get('webrtc.enableAudio')
        }
        this.constraints = constraints

        rtc.getSources(function(sources) {
            // add video
            var cameraName = config.get('webrtc.camera')
            var found = false
            App.log.info('Looking for camera: ' + cameraName)
            for (var i = 0; i < sources.length; i++) {
                var source = sources[i];
                if (source.label == cameraName || cameraName == 'default') {
                    App.log.info('Found camera: ' + source.label)
                    found = true
                    constraints.video =  {
                        'optional': [ {
                            'minWidth': config.get('webrtc.cameraWidth'),
                            'minHeight': config.get('webrtc.cameraHeight'),
                            'sourceId': source.id
                        }]
                    }
                    constraints.video = true;
                    break;
                }
            }

            if (!found) {
                App.log.info('"' + cameraName + '" not found')
            }
        })

        this.constraints = constraints;
        rtc.getUserMedia(constraints, function (stream) {
            self.mediaStream = stream
        })

    }

    resetStream() {
        var self = this;
        _.forEach(self.state.pcs, function(pco, id) {
            pco.pc.removeStream(self.mediaStream)
        })
        rtc.getUserMedia(self.constraints, function (stream) {
            self.mediaStream = stream;
    
            _.forEach(self.state.pcs, function(pco, id) {
                pco.pc.addStream(self.mediaStream)
            })
        })
    }

    initJumpChat() {
        if (!config.get('jumpchat.enabled'))
            return;

        var jc = new jcclient.JCClient({
            'serverName': 'https://jump.chat',
            'apiKey': 'db763c63-8e61-43a1-9f8c-f6c9ec9d27f8',
            'localStream': this.mediaStream
        })

        var url = config.get('jumpchat.url');
        var password = config.get('jumpchat.password');
        jc.joinUrl(url, password, function() {
            console.log('joined');
        });

        this.jc = jc;
    }

    initJanus() {
        if (!config.get('janus.enabled'))
            return;
        var options = {
            url: config.get('janus.url'),
            roomId: config.get('janus.roomId'),
            username: config.get('janus.username') || 'anon',
            localStream: this.mediaStream,
            videoType: config.get('janus.videoType')
        }

        this.janus = new JanusClient(options);
    }

    static initLogger() {
        if (!App.log) {
            // setup logger
            winston.level = config.get('server.loglevel')
            App.log = new winston.Logger({
                transports: [
                    new winston.transports.Console({
                        level: config.get('server.loglevel'),
                        handleExceptions: true,
                        json: false,
                        colorize: true
                    })
                ],
                exitOnError: false
            })
            App.stream = {
                write: function(message, encoding){
                    App.log.info(message);
                }
            }

            // webrtc logger
            var RTCPeerConnection = rtc.RTCPeerConnection
            rtc.setDebug(config.get('webrtc.loglevel'))
        }
    }

    initExpress() {

        // serve static
        this.server.use(express.static(config.get('server.staticroot')))
        this.server.use(morgan("combined", { "stream": App.stream }))
        this.server.use(bodyParser.json('*/*'));

        // middleware to use for all requests
        this.router.use(function(req, res, next) {
            // handle options
            if (req.method == 'OPTIONS') {
                res.header('Allow', 'GET, PUT, OPTIONS, DELETE');
                res.header('Access-Control-Allow-Origin', '*')
                res.header('Access-Control-Allow-Methods', 'GET, PUT, OPTIONS, DELETE');
                res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, X-API-Key');
                return res.send(200);
            }

            res.header('Access-Control-Allow-Origin', config.get('server.cors'))
            next(); // make sure we go to the next routes and don't stop here
        })

        var self = this
        this.router.route('/peerconnection')
            .get(function(req, res) {
                var pcs = []
                _.forEach(self.state.pcs, function(pco, id) {
                    pcs.push(self._pcoToJSON(id, pco))
                })

                res.json({ peerConnections: pcs })
            })
            .post(function(req, res) {
                var options = {
                    'iceServers': [
                        { 'url': ['stun:stun.l.google.com:19302'] },
                        // {
                        //     'url': 'turn:107.170.89.107',
                        //     'username': 'jumpchat',
                        //     'credential': 'E9kKsV0L9kxp8h5UcNIt'
                        // }
                    ]
                }

                App.log.info("pc constraints = ", self.constraints)
                var id = uuid()
                var pc = new RTCPeerConnection(options, self.constraints)
                var pco = {
                    'pc': pc,
                    'iceCandidates': []
                }
                self.state.pcs[id] = pco


                pc.addStream(self.mediaStream)

                pc.onicecandidate = function(e) {
                    var candidate = e.candidate;
                    if (candidate) {
                        var sdp = {
                            'candidate': candidate.candidate,
                            'sdpMid': candidate.sdpMid,
                            'sdpMLineIndex': candidate.sdpMLineIndex
                        }
                        pco.iceCandidates.push(sdp)
                    }
                }

                pc.onsignalingstatechange = function(state) {
                    App.log.debug('onsignalingstatechange = ' + pc.signalingState)
                    if (pc.signalingState === "closed") {
                        delete self.state.pcs[id];
                    }
                }

                pc.onicegatheringstatechange = function(state) {
                    App.log.debug('onicegatheringstatechange = ' + pc.iceGatheringState)
                }

                pc.oniceconnectionstatechange  = function() {
                    App.log.debug('oniceconnectionstatechange = ' + pc.iceConnectionState)

                    if (pc.iceConnectionState == 'failed') {
                        pc.close();
                        delete self.state.pcs[id];
                    }
                }

                pc.onnegotiationneeded = function() {
                    App.log.debug('onnegotiationneeded')
                }

                res.json({
                    'id': id
                })
            })

        this.router.route('/peerconnection/:id')
            .get(function(req, res) {
                //res.json({ message: 'pc! ' + req.params['id'] });
                var id = req.params['id']
                var pco = self.state.pcs[id]
                if (pco) {
                    res.json(self._pcoToJSON(id, pco))
                } else {
                    res.send(404)
                }
            })
            .put(function(req,res) {
                var id = req.params['id']
                var pco = self.state.pcs[id]
                var errorCb = function() {
                    App.log.debug('setRemoteDescription error');
                    res.json({})
                }
                if (pco) {
                    if (req.body.offer) {
                        var sdp = req.body.offer
                        pco.pc.setRemoteDescription(sdp, function() {
                            pco.pc.createAnswer(function(sdp) {
                                pco.pc.setLocalDescription(sdp, function() {
                                    App.log.debug('setLocalDescription success');
                                }, errorCb)
                                res.json({
                                    'result': 'success',
                                    'id': 'id',
                                    'answer': sdp
                                })
                            }, errorCb);
                        }, errorCb)
                    }


                    if (req.body.iceCandidate) {
                        var candidate = req.body.iceCandidate
                        pco.pc.addIceCandidate(new RTCIceCandidate(candidate))
                        res.json({})
                    }
                } else {
                    res.send(404);
                }
            })
            .delete(function(req,res) {
                var id = req.params['id']
                var pco = self.state.pcs[id];
                if (pco && pco.pc) {
                    pco.pc.close();
                }
                delete self.state.pcs[id]
                res.json({})
            })

        this.router.route('/get_video_info').get(function(req, res) {
            var id = req.query.video_id
            var url = 'https://www.youtube.com/get_video_info?video_id=' + id + '&el=info&ps=default&eurl=&gl=US&hl=en'
            fetch.fetchUrl(url, function(error, meta, body) {
                if (body) {
                    //res.body(body)
                    res.send(body.toString())
                }
            })
        })

        this.router.route('/reset').get(function(req,res) {
            self.resetStream();
            res.json({})            
        })

        // respond with "hello world" when a GET request is made to the homepage
        this.server.use('/api/v1/', this.router)
    }

    run() {
        var port = config.get('server.port', 8080)
        this.server.listen(port, function () {
            // Put a friendly message on the terminal
            App.log.info("Server running at http://0.0.0.0:" + port + "/")
        })
    }

    _pcoToJSON(id, pco) {
        if (!pco || !id) {
            return {}
        }

        var retval = {
            'id': id,
            'signalingState': _.get(pco, 'pc.signalingState'),
            'iceGatheringState': _.get(pco, 'pc.iceGatheringState'),
            'iceConnectionState': _.get(pco, 'pc.iceConnectionState'),
            'iceCandidates': _.get(pco, 'iceCandidates', [])
        }

        // clear ice iceCandidates
        pco.iceCandidates = []

        return retval
    }
}

module.exports = exports = { 'App': App }