

var rtc = require('webrtc-node');
var RTCPeerConnection = rtc.RTCPeerConnection;
var RTCSessionDescription = rtc.RTCSessionDescription;
var RTCIceCandidate = rtc.RTCIceCandidate;
var MediaStream = rtc.MediaStream;
var MediaStreamTrack = rtc.MediaStreamTrack;
var getUserMedia = rtc.getUserMedia;
var getSources = rtc.getSources;

var Janus = require('./janus');

class JanusClient {

    constructor(options) {
      this.state = {}
      options = options || {};
      this.options = options;
      this.state.url = options.url;
      this.state.username = options.username || 'anon';
      this.state.password = options.password || '';
      this.state.useOTG = options.useOTG || false;
      this.state.roomId = options.roomId || 9000;
      this.state.opaqueId = "videoroom-" + Janus.randomString(12);
      this.state.videoType = options.videoType;
      this.feeds = {};

      this.initWebRTC(options.success);
    }

    disconnect() {
        this.state.localStream.release();
        this.janus.destroy();
    }

    muteAudio(mute) {
      this.state.localStream.getAudioTracks().forEach((t) => {
        t.enabled = !mute;
      });
    }

    isAudioMuted() {
      var muted = false;
      this.state.localStream.getAudioTracks().forEach((t) => {
        muted = muted || !t.enabled;
      });
      return muted;
    }

    muteVideo(mute) {
      this.state.localStream.getVideoTracks().forEach((t) => {
        t.enabled = !mute;
      });
    }

    isVideoMuted() {
      var muted = false;
      this.state.localStream.getVideoTracks().forEach((t) => {
        muted = muted || !t.enabled;
      });
      return muted;
    }

    initWebRTC(cb) {
        let isFront = true;
        let self = this;
        if (this.options.localStream) {
          self.state.localStream = this.options.localStream;
          self.initJanus(self.state.url, cb);
        } else {
          MediaStreamTrack.getSources(sourceInfos => {
            console.log(sourceInfos);
            let videoSourceId = self.options.useOTG ? "UVCCamera" : undefined;
            let constraints = {
                audio: true,
                video: {
                mandatory: {
                    minWidth: 1280, // Provide your own width, height and frame rate here
                    minHeight: 720,
                    minFrameRate: 30
                },
                facingMode: (isFront ? "user" : "environment"),
                optional: (videoSourceId ? [{ sourceId: videoSourceId }] : [])
                }
            };
            getUserMedia(constraints, function (stream) {
                self.state.localStream = stream;
                self.initJanus(self.state.url, cb);
            }, console.error);
          });
        }
    }

  initJanus(url, cb) {
    let self = this;
    Janus.init({
      debug: "all", 
      callback: () => {
        self.janus = new Janus({
          server: url,
          success: () => {
            self.attachVideoRoom(cb);
          }
        });
      }
    });
  }

  unsubscribeFeed(feed) {
    let feedData = this.feeds[feed];
    if (feedData) {
      feedData.pluginHandle.hangup();
      delete this.feeds[feed];
    }
  }

  subscribeToFeeds(feeds) {
    let self = this;
    feeds.map(function(feed) {
      self.subscribeFeed(feed);
    });
  }

  subscribeFeed(feed) {
    // A new feed has been published, create a new plugin handle and attach to it as a listener
    var remoteFeed = null;
    let self = this;
    var currentStream = null;
    var currentFeedId = 0;
    this.janus.attach(
      {
        plugin: "janus.plugin.videoroom",
        opaqueId: this.state.opaqueId,
        success: function(pluginHandle) {
          currentFeedId = feed.id;
          self.feeds[feed.id] = {
            "feed": feed,
            "pluginHandle": pluginHandle
          }
          remoteFeed = pluginHandle;
          Janus.log("Plugin attached! (" + remoteFeed.getPlugin() + ", id=" + remoteFeed.getId() + ")");
          Janus.log("  -- This is a subscriber");
          // We wait for the plugin to send us an offer
          var listen = { "request": "join", "room": self.state.roomId, "ptype": "listener", "feed": feed.id };
          remoteFeed.send({"message": listen});
        },
        error: function(error) {
          Janus.error("  -- Error attaching plugin...", error);
        },
        ondata: function(data) {
          //console.log('Recv data: ' + JSON.stringify(data, null, 2));
        },
        ondataopen: function() {
          console.log("The publisher DataChannel is available");
          //connection.onDataOpen();
          let type = 'statusUpdate';
          let msg = {
            type: type,
            content: {
              source: self.myFeedId,
              status: {
                audioEnabled: self.isAudioMuted(),
                videoEnabled: self.isVideoMuted(),
                speaking: false,
                picture: null,
                display: self.state.username,
                videoType: self.state.videoType
              }
            }
          }

          let json = JSON.stringify(msg, null, 2);

            setTimeout(function() {
              self.videoRoom.data({
                text: json,
                error: function(reason) { console.error(reason); },
                success: function() { console.log("Data sent: " + type); }
              });
            }, 4000);
        },        
        onmessage: function(msg, jsep) {
          Janus.debug(" ::: Got a message (listener) :::");
          Janus.debug(JSON.stringify(msg));
          var event = msg["videoroom"];
          Janus.debug("Event: " + event);
          if(event != undefined && event != null) {
            if(event === "attached") {
              // Subscriber created and attached
              Janus.log("Successfully attached to feed " + remoteFeed.rfid + " (" + remoteFeed.rfdisplay + ") in room " + msg["room"]);
            } else if(msg["error"] !== undefined && msg["error"] !== null) {
              Janus.alert(msg["error"]);
            } else {
              // What has just happened?
            }
          }
          if(jsep !== undefined && jsep !== null) {
            Janus.debug("Handling SDP as well...");
            Janus.debug(jsep);
            // Answer and attach
            let media = {
              videoRecv: false, 
              audioRecv: false,
              audioSend: false,
              videoSend: false,
              data: true
            };

            remoteFeed.createAnswer(
              {
                jsep: jsep,
                // Add data:true here if you want to subscribe to datachannels as well
                // (obviously only works if the publisher offered them in the first place)
                media: media,
                success: function(jsep) {
                  Janus.debug("Got SDP!");
                  Janus.debug(jsep);
                  var body = { "request": "start", "room": self.state.roomId };
                  remoteFeed.send({"message": body, "jsep": jsep});
                },
                error: function(error) {
                  Janus.error("WebRTC error:", error);
                }
              });
          }
        },
        webrtcState: function(on) {
          Janus.log("Janus says this WebRTC PeerConnection (feed #" + remoteFeed.rfindex + ") is " + (on ? "up" : "down") + " now");
        },
        onlocalstream: function(stream) {
          // The subscriber stream is recvonly, we don't expect anything here
        },
        onremotestream: function(stream) {
          Janus.log(" ::: Got a remote stream " + stream.id + " :::");
          currentStream = stream;
          if (self.options.onaddstream) {
            self.options.onaddstream(currentStream);
          } else {
            stream.getVideoTracks().forEach(function(t) {
              t.enabled = false;
            });
            stream.getAudioTracks().forEach(function(t) {
              t.enabled = false;
            });
          }
        },
        oncleanup: function() {
          Janus.log(" ::: Got a cleanup notification (remote feed " + feed.id + ") :::");
          if (self.options.onremovestream) {
            self.options.onremovestream(currentStream);
          }
        }
      });
  }

  attachVideoRoom(cb) {
    let self = this;
    let videoRoomPlugin;
    self.janus.attach({
      plugin: "janus.plugin.videoroom",
      stream: self.state.localStream,
      opaqueId: this.state.opaqueId,
      success: function(pluginHandle) {
        // Step 1. Right after attaching to the plugin, we send a
        // request to join
        //connection = new FeedConnection(pluginHandle, that.room.id, "main");
        //connection.register(username);
        console.log("Plugin attached! (" + pluginHandle.getPlugin() + ", id=" + pluginHandle.getId() + ")");
        self.videoRoom = pluginHandle;
        videoRoomPlugin = pluginHandle;
        var register = { "request": "join", "room": self.state.roomId, "ptype": "publisher", "display": self.state.username };
-        pluginHandle.send({"message": register});
        if (cb) {
            cb(true);
        }
      },
      error: function(error) {
        //console.error("Error attaching plugin... " + error);

      },
      consentDialog: function(on) {
        console.log("Consent dialog should be " + (on ? "on" : "off") + " now");
        // $$rootScope.$broadcast('consentDialog.changed', on);
        // if(!on){
        //   //notify if joined muted
        //   if (startMuted) {
        //     $$rootScope.$broadcast('muted.Join');
        //   }
        // }
      },
      ondata: function(data) {
        console.log('Recv data: ' + JSON.stringify(data, null, 2));
      },
      ondataopen: function() {
        console.log("The publisher DataChannel is available");
        //connection.onDataOpen();
        let type = 'statusUpdate';
        let msg = {
          type: type,
          content: {
            source: self.myFeedId,
            status: {
              audioEnabled: self.isAudioMuted(),
              videoEnabled: self.isVideoMuted(),
              speaking: false,
              picture: null,
              display: self.state.username,
              videoType: self.state.videoType
            }
          }
        }

        let json = JSON.stringify(msg, null, 2);

        videoRoomPlugin.data({
          text: json,
          error: function(reason) { console.error(reason); },
          success: function() { console.log("Data sent: " + type); }
        });
      },
      onlocalstream: function(stream) {
        // Step 4b (parallel with 4a).
        // Send the created stream to the UI, so it can be attached to
        // some element of the local DOM
        console.log(" ::: Got a local stream :::");        
        // var feed = FeedsService.findMain();
        // feed.setStream(stream);
      },
      oncleanup: function () {
        console.log(" ::: Got a cleanup notification: we are unpublished now :::");
      },
      mediaState: function(medium, on) {
        console.log("Janus " + (on ? "started" : "stopped") + " receiving our " + medium);
      },
      webrtcState: function(on) {
        console.log("Janus says our WebRTC PeerConnection is " + (on ? "up" : "down") + " now");
      },
      onremotestream: function(stream) {
        console.log("Remote stream");
      },
      onmessage: function (msg, jsep) {
        var event = msg.videoroom;
        console.log("Event: " + event);

        // Step 2. Response from janus confirming we joined
        if (event === "joined") {
          console.log("Successfully joined room " + msg.room);
          self.myFeedId = msg.id;
          //ActionService.enterRoom(msg.id, username, connection);
          // Step 3. Establish WebRTC connection with the Janus server
          // Step 4a (parallel with 4b). Publish our feed on server

          // if (jhConfig.joinUnmutedLimit !== undefined && jhConfig.joinUnmutedLimit !== null) {
          //   startMuted = (msg.publishers instanceof Array) && msg.publishers.length >= jhConfig.joinUnmutedLimit;
          // }

          // connection.publish({
          //   muted: startMuted,
          //   error: function() { connection.publish({noCamera: true, muted: startMuted}); }
          // });

          let media = {
            videoRecv: false, 
            audioRecv: false,
            videoSend: true,
            audioSend: true,
            data: true,
            video: 'main'
          };

          self.videoRoom.createOffer({
            stream: self.state.localStream,
            media: media,
            success: function(jsep) {
              console.log("Got publisher SDP!");
              console.log(jsep);
              // that.config = new ConnectionConfig(pluginHandle, cfg, jsep);
              // // Call the provided callback for extra actions
              // if (options.success) { options.success(); }

              var publish = { "request": "configure", "audio": true, "video": true };
              self.videoRoom.send({"message": publish, "jsep": jsep});
              
            },
            error: function(error) {
              console.error("WebRTC error publishing");
              console.error(error);
              // // Call the provided callback for extra actions
              // if (options.error) { options.error(); }
            }
          });

          // // Step 5. Attach to existing feeds, if any
          if ((msg.publishers instanceof Array) && msg.publishers.length > 0) {
              msg.publishers.map(function(feed) {
                self.subscribeFeed(feed);
              });
          }
          // The room has been destroyed
        } else if (event === "destroyed") {
          console.log("The room has been destroyed!");
          //$$rootScope.$broadcast('room.destroy');
        } else if (event === "event") {
          // Any new feed to attach to?
          if ((msg.publishers instanceof Array) && msg.publishers.length > 0) {
              msg.publishers.map(function(feed) {
                self.subscribeFeed(feed);
              });            
          // One of the publishers has gone away?
          } else if(msg.leaving !== undefined && msg.leaving !== null) {
            var leaving = msg.leaving;
            self.unsubscribeFeed(leaving);
            //ActionService.destroyFeed(leaving);
          // One of the publishers has unpublished?
          } else if(msg.unpublished !== undefined && msg.unpublished !== null) {
            var unpublished = msg.unpublished;
            self.unsubscribeFeed(unpublished);
            //ActionService.destroyFeed(unpublished);
          // Reply to a configure request
          } else if (msg.configured) {
            // connection.confirmConfig();
          // The server reported an error
          } else if(msg.error !== undefined && msg.error !== null) {
            console.log("Error message from server" + msg.error);
            // $$rootScope.$broadcast('room.error', msg.error);
          }
        }

        if (jsep !== undefined && jsep !== null) {
          self.videoRoom.handleRemoteJsep({jsep: jsep});
        }
      }      
    })
  }

}

module.exports = exports = JanusClient;
