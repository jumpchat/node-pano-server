'use strict';

var rest = require('node-rest-client');
var rtc = require('webrtc-node');
var io = require('socket.io-client');

var RTCPeerConnection = rtc.RTCPeerConnection
var RTCSessionDescription = rtc.RTCSessionDescription
var RTCIceCandidate = rtc.RTCIceCandidate


var MediaStreamTrack = {
    'getSources': rtc.getSources
}

function Event(name) {
    this.name = name;
}

var JCClient = function(opts) {
    opts = opts || {};
    var serverName = opts.serverName || 'https://stage.jumpch.at';
    var serverUrl = serverName + '/v1';
    var users = {};
    var peers = {};
    var offer = {};
    var dcs = {}; // data channel
    var stream = {}; // streams
    var candidates = {};
    var files = {}; // received files
    var sendFiles = {}; // sent files
    var inRoom = false;
    var streamHD = false;
    var audioBandwidth = opts.audioBandwidth || 50;
    var videoBandwidth = opts.videoBandwidth || 300;
    var videoBandwidthHD = opts.videoBandwidthHD || 2000;
    var dataBandwidth = opts.dataBandwidth || 30*1000*100;
    var _apiKey = opts.apiKey;
    var _sharingScreen = false;
    var _callbacks = {};

    var _broadcastVideoShare = false;
    var _broadcastVideo = false;
    var _broadcastAudio = false;
    var _password = null;
    var _jumpchatExtensionId = 'dpolibhkbcepgolajbfjfcakgdkfkfjg';
    var _jumpchatExtensionLink = 'https://chrome.google.com/webstore/detail/dpolibhkbcepgolajbfjfcakgdkfkfjg';
    var _jumpchatAddonLink = 'https://addons.mozilla.org/firefox/downloads/latest/617104/addon-617104-latest.xpi';
    var _jumpchatExtensionInstalled = false;
    var _isFirefox = false;
    var _ua = "nodejs";
    var _isAndroid = false;
    var _localStorage  = {}
    var localStorage = {
        getItem: function(k) { 
            return _localStorage[k] 
        },
        setItem: function(k,v) { 
            _localStorage[k] = v
        }
    };
    this.restClient = new rest.Client();

    this.localStream = opts.localStream;

    _checkExtInstalled(function(installed) {
        _jumpchatExtensionInstalled = installed;
    });

    if (localStorage) {
        _broadcastVideo = localStorage.getItem('broadcastVideo') == null || localStorage.getItem('broadcastVideo') == 'true';
        _broadcastAudio = localStorage.getItem('broadcastAudio') == null || localStorage.getItem('broadcastAudio') == 'true';
    }

    function _guid() {
        function s4() {
            return Math.floor((1 + Math.random()) * 0x10000)
                 .toString(16)
                 .substring(1);
        }

        return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
    }

    function _fetchUserId() {
        if (opts.userid)
            return opts.userid;

        var userid = localStorage.getItem('userid');
        if (!userid) {
            userid = _guid();
            localStorage.setItem('userid', userid);
        }
        return userid;
    }

    function _findLine(sdpLines, prefix, substr) {
        return _findLineInRange(sdpLines, 0, -1, prefix, substr);
    }

    function _findLineInRange(sdpLines, startLine, endLine, prefix, substr) {
        var realEndLine = endLine !== -1 ? endLine : sdpLines.length;
        for (var i = startLine; i < realEndLine; ++i) {
            if (sdpLines[i].indexOf(prefix) === 0) {
                if (!substr || sdpLines[i].toLowerCase().indexOf(substr.toLowerCase()) !== -1) {
                    return i;
                }
            }
        }
        return null;
    }

    // Returns a new m= line with the specified codec as the first one.
    function setDefaultCodec(mLine, payload) {
        var elements = mLine.split(' ');

        // Just copy the first three parameters; codec order starts on fourth.
        var newLine = elements.slice(0, 3);

        // Put target payload first and copy in the rest.
        newLine.push(payload);
        for (var i = 3; i < elements.length; i++) {
            if (elements[i] !== payload) {
              newLine.push(elements[i]);
            }
        }
        return newLine.join(' ');
    }

    function findLine(sdpLines, prefix, substr) {
        return findLineInRange(sdpLines, 0, -1, prefix, substr);
    }

    function findLineInRange(sdpLines, startLine, endLine, prefix, substr) {
        var realEndLine = endLine !== -1 ? endLine : sdpLines.length;
        for (var i = startLine; i < realEndLine; ++i) {
            if (sdpLines[i].indexOf(prefix) === 0) {
                if (!substr || sdpLines[i].toLowerCase().indexOf(substr.toLowerCase()) !== -1) {
                    return i;
                }
            }
        }
        return null;
    }

    // Gets the codec payload type from sdp lines.
    function getCodecPayloadType(sdpLines, codec) {
        var index = findLine(sdpLines, 'a=rtpmap', codec);
        return index ? getCodecPayloadTypeFromLine(sdpLines[index]) : null;
    }

    // Gets the codec payload type from an a=rtpmap:X line.
    function getCodecPayloadTypeFromLine(sdpLine) {
        var pattern = new RegExp('a=rtpmap:(\\d+) \\w+\\/\\d+');
        var result = sdpLine.match(pattern);
        return (result && result.length === 2) ? result[1] : null;
    }

    function _maybePreferCodec(sdp, type, dir, codec) {
        var str = type + ' ' + dir + ' codec';
        if (codec === '') {
            console.log('No preference on ' + str + '.');
            return sdp;
        }

        console.log('Prefer ' + str + ': ' + codec);

        var sdpLines = sdp.split('\r\n');

        // Search for m line.
        var mLineIndex = findLine(sdpLines, 'm=', type);
        if (mLineIndex === null) {
            return sdp;
        }

        // If the codec is available, set it as the default in m line.
        var payload = getCodecPayloadType(sdpLines, codec);
        if (payload) {
            sdpLines[mLineIndex] = setDefaultCodec(sdpLines[mLineIndex], payload);
        }

        sdp = sdpLines.join('\r\n');
        return sdp;
    }

    function _setBandwidth(sdp) {
        //return sdp;

        sdp = sdp.replace( /b=AS([^\r\n]+\r\n)/g , '');
        if (audioBandwidth)
            sdp = sdp.replace(/a=mid:audio\r\n/g, 'a=mid:audio\r\nb=AS:' + audioBandwidth + '\r\n');
        if (videoBandwidth) {
            if (streamHD)
                sdp = sdp.replace(/a=mid:video\r\n/g, 'a=mid:video\r\nb=AS:' + videoBandwidthHD + '\r\n');
            else
                sdp = sdp.replace(/a=mid:video\r\n/g, 'a=mid:video\r\nb=AS:' + videoBandwidth + '\r\n');
        }
        if (dataBandwidth)
            sdp = sdp.replace( /a=mid:data\r\n/g , 'a=mid:data\r\nb=AS:' + dataBandwidth + '\r\n');
        return sdp;
    }

    function _base64toBlob(base64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 1024;
        var byteCharacters = atob(base64Data);
        var bytesLength = byteCharacters.length;
        var slicesCount = Math.ceil(bytesLength / sliceSize);
        var byteArrays = new Array(slicesCount);

        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
            var begin = sliceIndex * sliceSize;
            var end = Math.min(begin + sliceSize, bytesLength);

            var bytes = new Array(end - begin);
            for (var offset = begin, i = 0 ; offset < end; ++i, ++offset) {
                bytes[i] = byteCharacters[offset].charCodeAt(0);
            }
            byteArrays[sliceIndex] = new Uint8Array(bytes);
        }
        return new Blob(byteArrays, { type: contentType });
    }

    function _getICEServers(settings) {
        var iceServers = [
            {"url": "stun:stun.l.google.com:19302"}
        ];

        // process fetched turn servers first
        if (settings.turnIceServers) {
            console.log('adding turnIceServers: ' + JSON.stringify(settings.turnIceServers));
            iceServers = iceServers.concat(settings.turnIceServers);
        }

        iceServers = {
            iceServers: iceServers
        };

        return iceServers;
    }

    function _getOptions() {
        return  {
            optional: [
                {DtlsSrtpKeyAgreement: true}
                // data channels should be encrypted too
                //{RtpDataChannels: true}
            ],
            mandatory: { 'OfferToReceiveAudio': true, 'OfferToReceiveVideo': true }
        }
    }

    function _setChannelEvents(self, userid, channel) {
        channel.onmessage = function(e) {
            try {
                var msg = JSON.parse(e.data);
                if (msg.type == 'text') {
                    console.log('onmessage text: ' + JSON.stringify(msg));
                    _trigger('chatMessage', [userid, msg.msg])
                } else if (msg.type == 'file') {
                    var f = files[msg.id];
                    if (!f) {
                        f = files[msg.id] = {
                            'userid': userid,
                            'fileName': msg.fileName,
                            'mimeType': msg.mimeType,
                            'file': createJCFile(msg.fileName, msg.mimeType)
                        }
                    }
                    f.file.appendBase64(msg.data);

                    _trigger('fileProgress', [userid, msg.id, msg.progress, msg.sent, msg.total]);

                    if (msg.done) {
                        f.file.finish(function() {
                            _trigger('fileAdded', [userid, msg.id, f.file]);
                        });
                        delete files[msg.id];
                    }

                    if (msg.ack)
                        channel.send(JSON.stringify({type: 'file_ack', id: msg.id}));
                } else if (msg.type == 'file_ack') {
                    var filePack = sendFiles[msg.id];
                    if (filePack) {
                        filePack.lastCount = filePack.lastCountStart;
                        _sendPacket(self, userid, filePack);
                    }
                } else if (msg.type == 'file_prompt') {
                    console.log('onmessage file_prompt:');
                    _trigger('filePrompt', [userid, msg.file, function(answer) {
                        channel.send(JSON.stringify({ 'type':'file_prompt_answer', 'file':msg.file, 'answer':answer }));
                    }]);
                } else if (msg.type == 'file_prompt_answer') {
                    console.log('onmessage file_answer: ' + JSON.stringify(msg));
                    _trigger('filePromptAnswer', [userid, msg.answer, msg.file]);

                    if (msg.answer) {
                        // start file transfer
                        self._sendFile(userid, sendFiles[msg.file.id]);
                    }
                } else if (msg.type == 'file_cancel') {
                    self.cancelSendFiles(msg.id, true);
                    _trigger('fileCancel', [userid, msg.id]);
                } else if (msg.type == 'broadcast_state') {
                    console.log('onmessage broadcast_state: ' + JSON.stringify(msg));
                    users[userid].broadcastState = msg.state;
                    _trigger('broadcastState', [userid, msg.state]);
                } else if (msg.type == 'update_name') {
                    console.log('onmessage update_name: ' + JSON.stringify(msg));
                    users[userid].name = msg.name;
                    _trigger('updateName', [userid, msg.name]);
                } else if (msg.type == 'update_avatar') {
                    console.log('onmessage update_avatar: ' + userid);
                    var u = users[userid];
                    if (msg.packetNum == 1)
                        u.avatarChunks = [];

                    u.avatarChunks.push(msg.avatar);

                    if (msg.maxPackets == msg.packetNum) {
                        u.avatar = u.avatarChunks.join('');
                        console.log('got avatar ' + u.avatar);
                        delete u['avatarChunks'];
                        _trigger('updateAvatar', [userid, u.avatar]);
                    }
                } else if (msg.type == 'joined') {
                    console.log('onmessage joined: ' + userid);
                    _trigger('joined', [userid]);
                } else if (msg.type == 'search') {
                    console.log('onmessage search: ' + userid);
                    _trigger('search', [userid, msg.q])
                } else if (msg.type == 'data_message') {
                    console.log('onmessage data_message: ' + userid);
                    _trigger('dataMessage', [userid, msg.msg])
                }
            } catch (e) {
                console.log(e);
            }
        };

        channel.onopen = function() {
            console.log('dc open');
            _sendBroadcastState(channel, _broadcastVideo, _broadcastAudio, self.screenStream != null);
            channel.send(JSON.stringify({ 'type':'update_name', 'name': self.username() }));

            var avatar = self.avatar();
            _sendAvatar(channel, avatar, function() {
                channel.send(JSON.stringify({ 'type':'joined' }))
            });
        };

        channel.onclose = function() {
            console.log('dc close');
        };

        channel.onerror = function() {
            console.log('dc error');
        };
    }

    function _startPeerConnection(self, userid) {

        if (self.roomSettings.mode == 'mcu') {
            if (userid == 'mcu') {
                // publisher
                pc = peers[userid] = self.createPeerConnection(userid);
                _startOffer(self, pc);
            } else {
                // subscriber
                pc = peers[userid] = self.createPeerConnection(userid);
                var dc = dcs[userid] = pc.createDataChannel('jumpchat');
                _setChannelEvents(self, userid, dc);

                offer[userid] = true;
                _startOffer(self, pc);
            }
        } else {
            var pc;
            if (peers[userid]) {
                pc = peers[userid];
                if (self.screenStream)
                    pc.addStream(self.screenStream);
                if (self.localStream) {
                    pc.addStream(self.localStream);
                    if (self.localStream2)
                        peer.addStream(this.localStream2);
                }
            } else {
                pc = peers[userid] = self.createPeerConnection(userid);
            }
            var dc = dcs[userid] = pc.createDataChannel('jumpchat');
            _setChannelEvents(self, userid, dc);

            offer[userid] = true;
            _startOffer(self, pc);
        }
    }

    function _getVideoConstraints(type, videoSourceId, audioSourceId) {
        var constraints;

        if (type == 'hd') {
            constraints = {
                audio: true,
                video: _broadcastVideo ? {
                    mandatory: {
                        minWidth: 1280,
                        minHeight: 720
                    }
                } : false
            }
        } else  {
            constraints = {
                audio: true,
                video: _broadcastVideo ? {
                    mandatory: {
                        maxWidth: 640,
                        maxHeight: 480
                    }
                } : false
            }

        }

        // check for stream
        var videoSourceId = videoSourceId || localStorage.getItem('videoSourceId');
        var audioSourceId = audioSourceId || localStorage.getItem('audioSourceId');
        if (videoSourceId) {
            constraints.video.optional = [{
                sourceId: videoSourceId
            }]
        }

        if (audioSourceId) {
            constraints.audio = {
                optional: [{
                    sourceId: audioSourceId
                }]
            };
        }

        return constraints;
    }

    function _startOffer(self, peer) {
        var to = '';
        for (var i in peers) {
            if (peer == peers[i]) {
                to = i;
                break;
            }
        }

        peer.createOffer(function(desc) {
            console.log('offer created');
            //desc.sdp = _preferAudioCodec(desc.sdp, "ISAC/16000");
            desc.sdp = _setBandwidth(desc.sdp);
            desc.sdp = _maybePreferCodec(desc.sdp, 'video', 'send', 'H264/90000');
            desc.sdp = _maybePreferCodec(desc.sdp, 'video', 'receive', 'H264/90000');
            peer.setLocalDescription(desc, function() {
                var msg = {'to': to, 'sdp': { 'type': peer.localDescription.type, 'sdp':peer.localDescription.sdp } };
                console.log('offer: ' + JSON.stringify(msg));
                self.socket.emit('webrtc', msg);
            }, console.error, _getOptions());
        }, function(e) { console.log(e); }, _getOptions());
    }

    this.createPeerConnection = function(to) {
        var iceServers = _getICEServers(this.roomSettings);
        var peer = new RTCPeerConnection(iceServers);
        var self = this;
        peer.userid = to;

        if (this.roomSettings.mode != 'mcu' || to == 'mcu') {
            if (this.screenStream)
                peer.addStream(this.screenStream);
            else if (this.localStream) {
                peer.addStream(this.localStream);
                if (this.localStream2)
                    peer.addStream(this.localStream2);
            }
        }

        peer.onicecandidate = function (e) {
            console.log('onicecandidate ' + JSON.stringify(e.candidate));
            if (self.roomSettings.mode == 'mcu') {
                if (e.candidate == null)
                    self.socket.emit('webrtc', {'to': to.toString(), 'candidate': {'completed': true} })
                else
                    self.socket.emit('webrtc', {'to': to.toString(), 'candidate': e.candidate })
            } else {
                if (e.candidate)
                    self.socket.emit('webrtc', {'to': to.toString(), 'candidate': e.candidate });
            }
        };

        // once remote stream arrives, show it in the remote video element
        peer.onaddstream = function (e) {
            console.log('onaddstream');
            // var stream = e.stream;

            // var video = document.createElement('video');
            // video.autoplay = true;
            // stream[to] = stream;
            // //video.src = window.URL.createObjectURL(e.stream);
            // video.setAttribute('stream', to);
            // _trigger('remoteVideoAdded', [to, stream.id, video]);
            // attachMediaStream(video, stream);
            // video.autoplay = true;
            // //video.play();

            // if (e.stream.getVideoTracks().length == 0) {
            //     _trigger('broadcastState', [to, { 'video': false, 'audio': e.stream.getAudioTracks().length > 0 }]);
            // }
        };

        peer.onremovestream = function(e) {
            console.log('remove stream');
            _trigger('remoteVideoRemoved', [to, e.stream.id]);
        };

        peer.oniceconnectionstatechange = function(e) {
            console.log('oniceconnectionstatechange = ' + peer.iceConnectionState);
        };

        peer.onsignalingstatechange = function(e) {
            console.log('onsignalingstatechange = ' + peer.signalingState);
            if (peer.signalingState == 'closed') {
                peers[to] = null;
            }
        }

        peer.ondatachannel = function(e) {
            console.log('got data channel');
            var dc = dcs[to] = e.channel;
            _setChannelEvents(self, to, dc);
        };

        candidates[to] = [];

        return peer;
    };

    this.joinUrl = function(url, password, success, fail) {
        var self = this;
        this.restClient.get(url + '/settings', {
            'headers': { 'X-API-Key': _apiKey }
        }, function (data) {
            if (data['serverUrl'] != '')
                serverUrl = data['serverUrl'] + '/v1';
            self.roomSettings = data;
            var room = self.roomSettings['room'];
            var type = self.roomSettings['type'];
            self._join(room, type, password, success, fail);
        });
    }

    this.join = function(room, type, password, success, fail) {
        // fetch
        var self = this;
        console.log('joining ' + serverName + '/api/1/settings/' + room);
        this.restClient.get(serverName + '/api/1/settings/' + room, {}, function(data) {
            if (data['serverUrl'] != '')
                serverUrl = data['serverUrl'] + '/v1';
            //window.turnIceServers = data['turnIceServers'];
            self.roomSettings = data;
            self._join(room, type, password, success, fail);
        });
    }

    this._join = function(room, type, password, success, fail) {
        var self = this;
        var socket = io.connect(serverUrl, {'force new connection': opts.forceNew});
        this.socket = socket;
        this.room = room;
        this.roomType = type;
        var userid = _fetchUserId();
        this.userid = userid;

        socket.on('connect', function() {
            console.log('on_connect');
            socket.emit('join', { 'room': room, 'userid': userid, 'password':password, 'type': type, 'apiKey': _apiKey });
            inRoom = true;

            if (self.roomSettings.mode == 'mcu') {
                _startPeerConnection(self, 'mcu');
            }
        });

        socket.on('disconnect', function() {
            console.log('on_disconnect');
        });

        socket.on('users', function(data, sessid) {
            console.log('users: ' + JSON.stringify(data));
            //users = $.extend(users, data);
            self.sessid = sessid;
            for (var i in data) {
                var u = data[i];
                users[u.userid] = u;

                // if MCU mode, we create a connection to users listed
                if (self.roomSettings.mode == 'mcu') {
                    if (sessid != u.userid)
                        _startPeerConnection(self, u.userid);
                }
            }

            // start broadcasting
            self.startBroadcasting();

            if (success)
                success(users);
        });

        socket.on('joined', function(data) {
            console.log('joined: ' + JSON.stringify(data));
            var userid = data.userid;
            users[userid] = data;
            data.newUser = true;

            // start connection
            _startPeerConnection(self, userid);
        });

        socket.on('left', function(data) {
            console.log('left: ' + JSON.stringify(data));
            var userid = data.userid;

            if (peers[userid] == undefined)
                return;

            //if (self.remoteVideoRemovedCallback)
            //    self.remoteVideoRemovedCallback(userid);
            _trigger('remoteVideoRemoved', [userid]);
            _trigger('userLeft', [userid]);
            delete users[userid];

            var peer = peers[userid];
            if (peer)
                peer.close();
            delete peers[userid];

            delete dcs[userid];
            delete stream[userid];

            // clean up files
            var keys = Object.keys(sendFiles);
            for (var i in keys) {
                var k = keys[i];
                var fp = sendFiles[k];
                if (fp.userid == userid) {
                    delete sendFiles[k];
                    _trigger('fileCancel', [userid, k]);
                }
            }

            keys = Object.keys(files);
            for (var i in keys) {
                var k = keys[i];
                var fp = files[k];
                if (fp.userid == userid) {
                    delete files[k];
                    _trigger('fileCancel', [userid, k]);
                }
            }
        });

        socket.on('room', function(data) {
            _password = data.password;
            _trigger('room', [data]);
            console.log('room: ' + JSON.stringify(data));
        });

        socket.on('kicked', function(data) {
            // kicked out
            _trigger('kicked', [data]);
        });

        socket.on('locked', function(data) {
            // room is locked and gave wrong password
            _trigger('locked', [data]);
        });

        socket.on('knocking', function(data) {
            _trigger('knocking', [data]);
        });

        socket.on('knockAnswer', function(data) {
            _trigger('knockAnswer', [data]);
        });

        socket.on('systemMessage', function(data) {
            _trigger('systemMessage', [data]);
        });

        socket.on('webrtc', function(data) {
            console.log('webrtc: ' + JSON.stringify(data));
            var from = data['from'];

            var pc = peers[from];
            if (!pc)
                pc = peers[from] = self.createPeerConnection(from);

            if (data['candidate']) {
                if (candidates[from]) {
                    candidates[from].push(data['candidate']);
                } else {
                    var pc = peers[from];
                    pc.addIceCandidate(new RTCIceCandidate(data['candidate']),
                        function() { }, // success
                        function(e) { console.error(e); } // fail
                    );
                }

            } else if (data['sdp']) {
                offer[from] = false;
                pc.setRemoteDescription(new RTCSessionDescription(data.sdp), function () {
                    console.log('remote desc set');
                    if (pc.remoteDescription.type == 'offer') {
                        pc.createAnswer(function(desc) {
                            desc.sdp = _setBandwidth(desc.sdp);
                            desc.sdp = _maybePreferCodec(desc.sdp, 'video', 'send', 'H264/90000');
                            desc.sdp = _maybePreferCodec(desc.sdp, 'video', 'receive', 'H264/90000');
                            pc.setLocalDescription(desc, function() {
                                var msg = {'to': from, 'sdp': { 'type':pc.localDescription.type, 'sdp':pc.localDescription.sdp } };
                                console.log('answer: ' + JSON.stringify(msg));
                                socket.emit('webrtc', msg);
                            }, console.error);
                        }, console.error, _getOptions());
                    }

                    self.drainCandidates(from, pc);
                }, function(e) {
                    console.error(e);
                });
            }
        });
    };

    this.disconnect = function() {
        console.log('rtc.disconnect');

        var self = this;
        if (self.socket)
            self.socket.emit('leave');

        for (var id in peers) {
            //self.remoteVideoRemovedCallback(id);
            _trigger('remoteVideoRemoved', [id]);

            // close peers
            var p = peers[id];
            if (p)
                p.close();

            // stop streams
            var s = stream[id];
            if (s) {
                var audio = s.getAudioTracks();
                for (var i = 0; i < audio.length; i++) {
                    audio[i].stop();
                }

                var video = s.getVideoTracks();
                for (var i = 0; i < video.length; i++) {
                    video[i].stop();
                }
            }
        }

        // reset structures
        users = {};
        peers = {};
        dcs = {}; // data channel
        stream = {}; // streams
        inRoom = false;
        return true;
    };

    this.drainCandidates = function(userid, pc) {
        console.log('drain candidates ' + userid);
        var iceCandidates = candidates[userid];
        for (var i in iceCandidates) {
            pc.addIceCandidate(new RTCIceCandidate(iceCandidates[i]),
                function() { }, // success
                function(e) { console.error(e); } // fail
            );
        }
        delete candidates[userid];
    };

    this.startBroadcasting = function() {
        console.log('start broadcasting');
        this.broadcasting = true;
    };

    this.stopBroadcasting = function() {
        this.broadcasting = false;
        peers = {};
    };

    function _processLocalStream(self, stream, cb) {
        var video = document.createElement('video');
        video.autoplay = true;

        // update after in the DOM
        self.localStream = stream;
        var audioTracks = self.localStream.getAudioTracks();
        if (audioTracks.length > 0)
            audioTracks[0].enabled = _broadcastAudio;
        else
            _broadcastAudio = false;

        var videoTracks = self.localStream.getVideoTracks();
        if (videoTracks.length > 0)
            videoTracks[0].enabled = _broadcastVideo;
        else
            _broadcastVideo = false;

        stream['me'] = self.localStream;
        //video.src = window.URL.createObjectURL(stream);
        video.autoplay = true;
        video.muted = true;
        if (cb)
            cb(video);
        attachMediaStream(video, stream);
        //video.play();
    }

    this.enableHD = function(val, cb) {
        if (val != undefined) {
            var prevVal = localStorage.getItem('hd') == 'true';
            if (prevVal != val) {
                localStorage.setItem('hd', val);
                this.localStream.getVideoTracks()[0].stop();
                this.localStream = null;
                var self = this;
                // create local video
                this.localVideo(function(v) {
                    _trigger('localVideoUpdated', [self.localStream]);
                    for (var i in peers) {
                        var pc = peers[i];
                        pc.removeStream(self.localStream);
                        _startOffer(self, pc);
                        _resetLocalStream(self, pc, self.localStream);
                    }

                    if (cb)
                        cb(localStorage.getItem('hd') == 'true');
                });
            }
        }
        var result = localStorage.getItem('hd');
        return result == 'true';
    }

    this.enable2ndVideo = function(val, cb) {
        if (val != undefined) {
            var prevVal = localStorage.getItem('video2');
            if (prevVal != val) {
                localStorage.setItem('video2', val);
            }
        }
        return localStorage.getItem('video2') == 'true';
    }

    this.resetLocalVideo = function(cb) {
        if (this.localStream && this.localStream.getVideoTracks().length == 1)
            this.localStream.getVideoTracks()[0].stop();
        this.localStream = null;
        var self = this;
        // create local video
        this.localVideo(function(v) {
            _trigger('localVideoUpdated', [self.localStream]);
            for (var i in peers) {
                var pc = peers[i];
                pc.removeStream(self.localStream);
                _startOffer(self, pc);
                _resetLocalStream(self, pc, self.localStream);
            }
            if (cb)
                cb();
        });
    }

    this.setAudioSourceId = function(sourceId) {
        if (sourceId == null || sourceId == '') {
            localStorage.removeItem('audioSourceId');
        } else {
            localStorage.setItem('audioSourceId', sourceId);
        }
    }

    this.setVideoSourceId = function(sourceId) {
        if (sourceId == null || sourceId == '') {
            localStorage.removeItem('videoSourceId');
        } else {
            localStorage.setItem('videoSourceId', sourceId);
        }
    }

    this.localVideo = function(cb) {
        var self = this;
        if (this.localStream) {
            // var video = document.createElement('video');
            // video.autoplay = true;

            // // update after in the DOM
            // stream['me'] = self.localStream;
            _addLocalVideo(this.localStream, cb, 'local');
            return;
        }

        var type = localStorage.getItem('hd') == 'true' ? 'hd' : 'sd';
        var constraints = _getVideoConstraints(type);

        rtc.getUserMedia(constraints, function(stream) {
            streamHD = type == 'hd';
            _processLocalStream(self, stream, cb);
        }, function(e) {
            if (type == 'hd') {
                streamHD = false;
                constraints = _getVideoConstraints('sd');

                // try non hd
                rtc.getUserMedia(constraints, function (stream) {
                    _processLocalStream(self, stream, cb);
                }, function (e) {

                    // try audio only
                    constraints.video = false;
                    rtc.getUserMedia(constraints, function (stream) {
                        _broadcastVideo = false;
                        _processLocalStream(self, stream, cb);
                    }, function (e) {
                        _broadcastAudio = false;
                        _broadcastVideo = false;
                        if (cb)
                            cb(null);
                    });

                });
            } else {
                // try audio only
                constraints.video = false;
                rtc.getUserMedia(constraints, function (stream) {
                    _broadcastVideo = false;
                    _processLocalStream(self, stream, cb);
                }, function (e) {
                    _broadcastAudio = false;
                    _broadcastVideo = false;
                    if (cb)
                        cb(null);
                });
            }
        });
    }

    this.add2ndVideo = function(source, cb) {
        if (!this.localStream)
            return false;

        var type = localStorage.getItem('hd') == 'true' ? 'hd' : 'sd';
        var constraints = _getVideoConstraints(type);
        constraints.audio = false;
        constraints.video.optional = [{
            sourceId: source
        }];

        var self = this;

        rtc.getUserMedia(constraints, function(stream) {
            var video = document.createElement('video');
            video.autoplay = true;
            video.muted = true;
            if (cb)
                cb(video);
            attachMediaStream(video, stream);
            //video.play();

            self.localStream2 = stream;

            for (var i in peers) {
                var pc = peers[i];
                pc.addStream(stream);
                _startOffer(self, pc);
            }

        }, function(e) {
            cb(null);
        });
    }

    this.remove2ndVideo = function() {
        if (this.localStream2) {
            var self = this;
            for (var i in peers) {
                var pc = peers[i];
                pc.removeStream(this.localStream2);
                _startOffer(self, pc);
            }
            this.localStream2 = null;

        }
    }

    // callbacks
    this.on = function(name, cb) {
        _callbacks[name] = cb;
    }

    function _trigger(name, data) {
        var cb = _callbacks[name];
        var e = new Event(name);
        data = data.slice(0);
        data.unshift(e);
        if (typeof(cb) == "function")
            cb.apply(e, data);
    }

    this.username = function(name) {
        if (name || name == '') {
            localStorage.setItem('username', name);
            for (var i in dcs) {
                var dc = dcs[i];
                dc.send(JSON.stringify({ 'type':'update_name', 'name': name }));
            }
        }
        return localStorage.getItem('username') || '';
    }

    function _sendAvatar(dc, avatar, cb) {
        var packetSize = 15*1000;
        var maxPackets = parseInt(avatar.length/packetSize) + 1;
        var i = 0;
        function sendAvatarPacket() {
            var chunk = avatar.slice(0, packetSize);
            avatar = avatar.slice(packetSize);
            dc.send(JSON.stringify({ 'type':'update_avatar', 'packetNum':i+1, 'avatar': chunk, 'maxPackets': maxPackets }));
            i++;
            if (avatar.length > 0)
                setTimeout(sendAvatarPacket, 100);
            else if (cb)
                cb();
        }
        sendAvatarPacket();
    }

    this.avatar = function(avatar) {
        if (avatar) {
            localStorage.setItem('avatar', avatar);

            for (var i in dcs) {
                var dc = dcs[i];
                _sendAvatar(dc, avatar);
            }
            _trigger('updateAvatar', ['me', avatar]);
        }
        //return localStorage.getItem('avatar') || window.staticUrl + 'img/default-avatar.png';
        return "";
    }

    this.user = function(userid) {
        if (userid == 'me') {
            userid = this.sessid;
            var user = users[userid] || {};
            user.avatar = this.avatar();
            user.name = this.username();
            return user;
        }
        return users[userid] || {};
    }

    this.isSupported = function() {
        return rtc.getUserMedia != null;
    }

    this.sendChatMessage = function(msg, userid) {
        console.log('sending msg: ' + msg);

        if (userid) {
            if (dcs[userid]) {
                dcs[userid].send(JSON.stringify({'type': 'text', 'msg':msg}));
            }
        } else {
            for (var uid in peers) {
                if (dcs[uid])
                    dcs[uid].send(JSON.stringify({ 'type': 'text', 'msg': msg }));
            }
        }
    }

    this.sendFiles = function(userid, files) {
        // prompt
        var totalSize = 0;
        var names = [];
        var sizes = [];
        var fileList = [];
        for (var i = 0; i < files.length; i++) {
            names.push(files[i].name);
            sizes.push(files[i].size);
            fileList.push(files[i]);
        }

        var file = {
            'id': _guid(),
            'files': fileList,
            'userid': userid
        };
        sendFiles[file.id] = file;
        if (dcs[userid])
            dcs[userid].send(JSON.stringify({ 'type':'file_prompt', 'file': { 'id': file.id, 'names': names, 'sizes': sizes } }));

        return file.id;
    }

    this.cancelSendFiles = function(userid, fileId, recvCancelMessage) {
        var filePack = sendFiles[fileId];
        if (filePack) {
            filePack.cancel = true;
            delete sendFiles[fileId];
        }

        delete files[fileId];

        if (!recvCancelMessage) {
            var userid = userid;
            if (dcs[userid])
                dcs[userid].send(JSON.stringify({ 'type': 'file_cancel', 'id': fileId }));
        }
    }

    // send a chunk of the file
    function _sendPacket(self, userid, filePack) {
        // stop sending the packet
        if (filePack.cancel)
            return;

        var f = filePack.files[filePack.index];
        var blob = f.file.slice(filePack.offset, filePack.offset + filePack.sliceSize);
        var reader = new FileReader();
        var sizeSent = 0;
        for (var i = 0; i < filePack.index; i++)
            sizeSent += filePack.files[i].size;
        reader.readAsBinaryString(blob);
        reader.onloadend = function() {
            try {
                filePack.offset += blob.size;
                sizeSent += filePack.offset;
                var byteCharacters = btoa(reader.result);
                var progress = parseInt(sizeSent*100/filePack.totalSize);
                filePack.progress = progress;
                dcs[userid].send(JSON.stringify({
                    type: 'file',
                    id: filePack.id,
                    fileName: f.name,
                    mimeType: f.type,
                    ack: filePack.lastCount == 0,
                    progress: progress,
                    sent: sizeSent,
                    total: filePack.total,
                    done: blob.size  < filePack.sliceSize,
                    data: byteCharacters
                }));


                _trigger('fileProgress', [userid, filePack.id, progress, sizeSent, filePack.totalSize]);

                filePack.lastCount--;
                if (filePack.lastCount >= 0)
                    setTimeout(function() {
                        _sendPacket(self, userid, filePack);
                    }, 0);
            } catch(e) {
                console.log(e);
            }

            var sendAgain = false;
            if (blob.size < filePack.sliceSize) {
                filePack.index++;
                filePack.offset = 0;
            }

            if (filePack.index < filePack.files.length)
                filePack.sendAgain = true;
        }
    }

    // really send the file
    this._sendFile = function(userid, filePack) {
        var sliceSize = 15*1024*(2/3);
        var totalSize = 0;
        for (var i = 0; i < filePack.files.length; i++) {
            totalSize += filePack.files[i].size;
        }

        filePack.totalSize = totalSize;
        filePack.index = 0;
        filePack.offset = 0;
        filePack.sliceSize = sliceSize;
        // packets before expecting an ack
        filePack.lastCountStart = 20;
        filePack.lastCount = filePack.lastCountStart;

        _sendPacket(this, userid, filePack);
    }

    this.stream = function(userid) {
        if (userid == 'me')
            return this.localStream;
        return stream[userid];
    }

    this.peers = function() {
        return peers;
    }

    this.connected = function() { return inRoom };

    this.users = function() {
        return users;
    }

    this.admin = function() {
        var user = users[this.sessid];
        if (user)
            return user.admin;
        return false;
    }

    function _sendBroadcastState(dc, video, audio, screenShare) {
        if (dc)
            dc.send(JSON.stringify({ 'type':'broadcast_state', 'state': {'video': video, 'audio': audio, 'screenShare': screenShare } }));
    }

    this.broadcastAudio = function(val) {
        if (val != null) {
            if (this.localStream) {
                var audioTracks = this.localStream.getAudioTracks();
                audioTracks = audioTracks.concat(this.screenStream ? this.screenStream.getAudioTracks() : []);
                if (audioTracks.length > 0) {
                    _broadcastAudio = val;
                    localStorage.setItem('broadcastAudio', _broadcastAudio);
                    for (var i = 0; i < audioTracks.length; i++)
                        audioTracks[i].enabled = _broadcastAudio;
                } else {
                    _broadcastAudio = false;
                }
            } else {
                _broadcastAudio = false;
            }

            for (var userid in peers) {
                _sendBroadcastState(dcs[userid], _broadcastVideo, _broadcastAudio, this.screenStream != null);
            }
            _trigger('broadcastState', ['me', { 'video': _broadcastVideo, 'audio': _broadcastAudio, 'screenShare': this.screenStream != null }]);
        }
        return _broadcastAudio;
    }

    this.broadcastVideo = function(val, force) {
        if (_broadcastVideo == val && force == null)
            return;
        if (val != null) {
            if (this.localStream) {
                var videoTracks = this.localStream.getVideoTracks();
                _broadcastVideo = val;
                localStorage.setItem('broadcastVideo', _broadcastVideo);
                if (true) {
                    if (videoTracks[0])
                        videoTracks[0].enabled = _broadcastVideo;
                } else
                if (_broadcastVideo) {
                    if (this.screenStream) {
                        this.screenStream.getVideoTracks()[0].enabled = true;
                    } else {
                        var constraints = {
                            audio: true,
                            video: {
                                mandatory: {
                                    maxWidth: 1280,
                                    maxHeight: 720
                                }
                            }
                        }

                        var self = this;
                        for (var i in peers) {
                            var pc = peers[i];
                            pc.removeStream(self.localStream);
                        }

                        rtc.getUserMedia(constraints, function(stream) {
                            stream.getAudioTracks()[0].enabled = _broadcastAudio;
                            self.localStream = stream;
                            _trigger('localVideoUpdated', [self.localStream]);
                            for (var i in peers) {
                                var pc = peers[i];
                                pc.addStream(self.localStream);
                                _startOffer(self, pc);
                                _resetLocalStream(self, pc, self.localStream);
                            }
                        }, function(e) {
                            console.error(e);
                        });
                    }

                } else {
                    if (this.screenStream) {
                        this.screenStream.getVideoTracks()[0].enabled = false;
                    } else {
                        var tracks = this.localStream.getVideoTracks();
                        if (tracks.length > 0) {
                            var track = tracks[0];
                            track.stop();
                            this.localStream.removeTrack(track);
                        }
                    }
                }
            } else {
                if (this.screenStream) {
                    _broadcastVideo = val;
                    this.screenStream.getVideoTracks()[0].enabled = _broadcastVideo;
                } else {
                    _broadcastVideo = false;
                }
            }

            var screenStream = this.screenStream != null;
            for (var userid in peers) {
                _sendBroadcastState(dcs[userid], _broadcastVideo, _broadcastAudio, screenStream);
            }
            _trigger('broadcastState', ['me', { 'video': _broadcastVideo, 'audio': _broadcastAudio, 'screenShare': screenStream }]);
        }
        return _broadcastVideo;
    }

    var _resetLocalStreamTimeout = null;
    function _resetLocalStream(self, pc, stream) {
        if (_resetLocalStreamTimeout != null)
            return;
        _resetLocalStreamTimeout = setTimeout(function() {
            if (pc.signalingState == 'stable' || pc.signalingState == 'have-local-offer') {
                console.log('_resetLocalStream startOffer()');
                pc.addStream(stream);
                _startOffer(self, pc);
                _resetLocalStreamTimeout = null;
            } else {
                console.log('_resetLocalStream waiting 500ms');
                clearTimeout(_resetLocalStreamTimeout);
                _resetLocalStreamTimeout = null;
                _resetLocalStream(self, pc, stream);
            }
        }, 1000);
    }
    function _shareScreen(self, shareScreen) {
        for (var i in peers) {
            var pc = peers[i];
            if (_isFirefox) {
                pc.close();
                peers[i] = self.createPeerConnection(i);
            }

            if (shareScreen) {
                if (!_isFirefox && self.localStream)
                    pc.removeStream(self.localStream);
                _startOffer(self, pc);
                _resetLocalStream(self, pc, self.screenStream);
            } else {
                // remove everything
                if (!_isFirefox)
                    pc.removeStream(self.screenStream);
                _startOffer(self, pc);
                _resetLocalStream(self, pc, self.localStream);
                self.screenStream = null;
            }
        }
    }

    function _addLocalVideo(stream, cb, type) {
        // create video
        var video = document.createElement('video');
        video.autoplay = true;
        //video.src = window.URL.createObjectURL(stream);
        video.setAttribute('stream', stream.id);
        cb(video, type);
        attachMediaStream(video, stream);
        //video.play();
    }

    function _checkExtInstalled(isInstalledCallback) {
        // extension not installed
        isInstalledCallback(false);
    }

    this.isSharingScreen = function() {
        return _sharingScreen;
    }

    this.shareScreen = function(cb, fail) {
        _sharingScreen = !_sharingScreen;
        var self = this;

        if (!_sharingScreen) {
            if (this.screenStream) {
                var videoTrack = this.screenStream.getVideoTracks()[0];
                // we need to clear this because it'll call disable screen share again
                videoTrack.onended = null;
                videoTrack.stop();
            }
            _shareScreen(this, _sharingScreen);

            if (this.localStream) {
                _addLocalVideo(this.localStream, cb, 'local');

                this.broadcastVideo(_broadcastVideoShare);
            } else {
                this.broadcastVideo(false);
            }
            return;
        }

        function _processScreenStream(constraints, moveAudio) {
            navigator.getUserMedia(constraints, function(stream) {
                // update after in the DOM
                self.screenStream = stream;

                // make sure audio is working
                if (moveAudio && self.localStream) {
                    var audioTracks = self.localStream.getAudioTracks();
                    if (audioTracks && audioTracks.length > 0) {
                        var audioTrack = self.localStream.getAudioTracks()[0];
                        stream.addTrack(audioTrack);
                    }
                }

                stream.getVideoTracks()[0].onended = function() {
                    console.log('stream ended');
                    self.shareScreen(cb);
                }

                _shareScreen(self, _sharingScreen);
                _addLocalVideo(stream, cb, 'screenshare');
                _broadcastVideoShare = _broadcastVideo;
                self.broadcastVideo(true, true);
            }, function(e) {
                _sharingScreen = false;
                fail(false);
            });
        }

        function _extentionInstalled() {

            if (_isFirefox) {
                var fullscreen = confirm("Do you want to share fullscreen?\n\nClick Ok to share full screen.\n\nClick Cancel to share windows.");
                var type = fullscreen ? "screen" : "window";
                var constraints = {
                    video: {
                        mozMediaSource: type,
                        mediaSource: type
                    },
                    audio: true
                }
                _processScreenStream(constraints, false);
            } else {
                chrome.runtime.sendMessage(
                    _jumpchatExtensionId,
                    {getStream: true},
                    function (response) {
                        if (!response) {
                            fail(chrome.runtime.lastError);
                            _jumpchatExtensionInstalled = false;
                            return;
                        }
                        console.log("Response from extension: " + JSON.stringify(response));
                        if (response.streamId) {
                            var constraints = {};
                            constraints.video = {
                                mandatory: {
                                    chromeMediaSource: "desktop",
                                    chromeMediaSourceId: response.streamId,
                                    maxWidth: window.screen.width,
                                    maxHeight: window.screen.height,
                                    maxFrameRate: 30
                                }
                            };
                            _processScreenStream(constraints, true);
                        } else {
                            _sharingScreen = false;
                            fail("Extension failed to get the stream");
                        }
                    });
            }
        }

        if (self.isExtensionInstalled())
            _extentionInstalled();
        else
            _trigger('noExtension');
    }

    this.isExtensionInstalled = function() {
        if (_isFirefox)
            _jumpchatExtensionInstalled = document.querySelectorAll(".jcffExtInstalled").length > 0;
        return _jumpchatExtensionInstalled;
    }

    this.installExtension = function(cb, fail) {
        if (_isFirefox) {
            setTimeout(function() {
                alert("You will need to reload after installing the addon")
                window.location.reload();
            }, 5000);
            window.location.href = _jumpchatAddonLink;
        } else {
            var self = this;
            chrome.webstore.install(
                _jumpchatExtensionLink,
                function (arg) {
                    console.log("Extension installed successfully", arg);
                    _jumpchatExtensionInstalled = true;
                    setTimeout(function () {
                        self.shareScreen(cb, fail);
                    }, 1000);
                },
                function (arg) {
                    fail(arg)
                    console.log("Failed to install the extension", arg);
                }
            );
        }
    }


    this.sendDebugLog = function(email, msg, log) {
        var ua = _ua;
        var info = {
            'email': email,
            'msg': msg,
            'ua': _ua,
            'log': log
        }
        this.socket.emit('debug_log', info);
    }

    this.lockRoom = function(locked) {
        this.socket.emit('lock', locked)
    }

    this.knock = function(msg) {
        this.socket.emit('knock', { 'room': this.room, 'type': this.roomType, 'msg': msg });
    }

    this.knockAnswer = function(sessid, allow) {
        this.socket.emit('knockAnswer', { 'sessid':sessid,
            'answer':allow });
    }

    this.password = function() {
        return _password;
    }

    this.kick = function(userid) {
        this.socket.emit('kick', userid);
    }

    this.getSources = function(cb) {
        if (MediaStreamTrack)
            MediaStreamTrack.getSources(function (media_sources) {
                cb(media_sources);
            });
        else
            cb([]);
    }

    this.doSearch = function(q) {
        console.log('search msg: ' + q);

        for (var userid in peers) {
            if (dcs[userid])
                dcs[userid].send(JSON.stringify({ 'type':'search', 'q':q }));
        }
    }

    this.sendDataMessage = function(msg, userid) {
        if (userid) {
            if (dcs[userid]) {
                dcs[userid].send(JSON.stringify({'type': 'data_message', 'msg':msg}));
            }
        } else {
            for (var uid in peers) {
                if (dcs[uid])
                    dcs[uid].send(JSON.stringify({'type': 'data_message', 'msg':msg}));
            }
        }

    }

    this.muteAudio = function(userid, mute) {
        var s = stream[userid];
        if (s)
            s.getAudioTracks()[0].enable = !mute;
    }

    this.muteVideo = function(userid, mute) {
        var s = stream[userid];
        if (s)
            s.getVideoTracks()[0].enable = !mute;
    }
};

module.exports = exports = {
    'JCClient': JCClient
}
