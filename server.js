// used to ignore certificate errors
process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";

var App = require('./lib/app').App
var app = new App()
app.parseArgs()
app.init()
app.run()
