var os = require('os')
var path = require('path')
var platform = os.platform()
var spawnSync = require('child_process').spawnSync

var version = '1.0.0.5'

if (platform == 'win32') {
    // create nsis
    var nsis_path = 'C:/Program Files (x86)/NSIS'
    var Registry = require('winreg')
    var reg = new Registry({
        hive: Registry.HKLM,
        key: '\\SOFTWARE\\WOW6432Node\\NSIS'
    })
    reg.values(function(err, items) {
        nsis_path = items[0].value

        var makensis_path = path.resolve(nsis_path, 'Bin', 'makensis.exe')

        console.log("cwd: " + path.dirname(__dirname))
        var res = spawnSync(makensis_path, ['/v4',  '/DVERSION='+version, 'scripts\\pack.nsi'], {
            cwd: path.dirname(__dirname),
            stdio: 'inherit'
        })
        
        // console.log(makensis_path)
    })

} else {
    // just tgz
    var res = spawnSync('/usr/bin/tar', [
            'zcvf',
            'node-pano-server-' + version + '.tgz',
            '--exclude="*.o"',
            'server.js',
            'package.json',
            'README.md',
            'LICENSE.txt',
            'config',
            'lib',
            'node_modules',
            'static'
        ], {
        cwd: path.dirname(__dirname),
        stdio: 'inherit'
    })
}