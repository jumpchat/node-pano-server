;--------------------------------
;Include Modern UI

  !include "MUI2.nsh"
  !include "FileFunc.nsh"
  !include "LogicLib.nsh"

;--------------------------------
;Defines
!define PRODUCT "Node.js Pano Server"
!define PUBLISHER "JumpChat"
!define ARP "Software\Microsoft\Windows\CurrentVersion\Uninstall\${PRODUCT}"

;--------------------------------
;Version Information

  VIProductVersion "${VERSION}"
  VIAddVersionKey "ProductName" "${PRODUCT}"
  VIAddVersionKey "FileDescription" "${PRODUCT}"
  VIAddVersionKey "FileVersion" "${VERSION}"

;--------------------------------
;General

  ;Name and file
  Name "${PRODUCT}"
  OutFile "..\node-pano-server-${VERSION}-installer.exe"

  ;Default installation folder
  InstallDir "$PROGRAMFILES\${PRODUCT}"
  
  ;Get installation folder from registry if available
  InstallDirRegKey HKCU "Software\${PRODUCT}" ""

  ;Request application privileges for Windows Vista
  RequestExecutionLevel admin

;--------------------------------
;Interface Settings

  !define MUI_ABORTWARNING

;--------------------------------
;Pages

  !insertmacro MUI_PAGE_LICENSE "..\LICENSE.txt"
  !insertmacro MUI_PAGE_COMPONENTS
  !insertmacro MUI_PAGE_DIRECTORY
  !insertmacro MUI_PAGE_INSTFILES
  
  !insertmacro MUI_UNPAGE_CONFIRM
  !insertmacro MUI_UNPAGE_INSTFILES
  
;--------------------------------
;Languages
 
  !insertmacro MUI_LANGUAGE "English"

;--------------------------------
;Installer Sections

Section "${PRODUCT}" SecDummy
  SetRegView 64

  SetOutPath "$INSTDIR"
  
  ;ADD YOUR OWN FILES HERE...
  File ..\server.js
  File ..\package.json
  File ..\README.md
  File ..\LICENSE.txt
  File /r ..\config
  File /r ..\lib
  File /r /x '*.obj' /x '*.o' /x '*.map' /x '*.pdb'  /x 'obj' /x '*.lib' \
    /x '*.html' /x 'node-gyp' ..\node_modules
  File /r ..\static

  ; create start menu
  CreateDirectory "$SMPROGRAMS\${PRODUCT}"
  Var /GLOBAL NODEJS
  StrCpy $NODEJS "C:\Program Files!!!\nodejs"
  ReadRegStr $0 HKLM "Software\Node.js" "InstallPath"
  ${If} $0 != ""
    StrCpy $NODEJS $0
  ${EndIf}
  CreateShortCut "$SMPROGRAMS\${PRODUCT}\${PRODUCT}.lnk" \
    "$NODEJS\node.exe" "$\"$INSTDIR\server.js$\""
  CreateShortCut "$SMPROGRAMS\${PRODUCT}\Uninstall.lnk" \
    "$INSTDIR\Uninstall.exe" "" "$INSTDIR\Uninstall.exe" 0
  
  ;Store installation folder
  WriteRegStr HKCU "Software\${PRODUCT}" "" $INSTDIR

  ;Create uninstaller
  WriteUninstaller "$INSTDIR\Uninstall.exe"
  WriteRegStr HKLM "${ARP}" "DisplayName" "${PRODUCT}"
  WriteRegStr HKLM "${ARP}" "DisplayVersion" "${VERSION}"
  WriteRegStr HKLM "${ARP}" "Publisher" "${PUBLISHER}"
  ${GetSize} "$INSTDIR" "/S=0K" $0 $1 $2
  IntFmt $0 "0x%08X" $0  
  WriteRegDWORD HKLM "${ARP}" "EstimatedSize" "$0"
  WriteRegStr HKLM "${ARP}" "UninstallString" "$\"$INSTDIR\uninstall.exe$\""
  WriteRegStr HKLM "${ARP}" "QuietUninstallString" "$\"$INSTDIR\uninstall.exe$\" /S"

SectionEnd

;--------------------------------
;Descriptions

  ;Language strings
  LangString DESC_SecDummy ${LANG_ENGLISH} "Server Sources."

  ;Assign language strings to sections
  !insertmacro MUI_FUNCTION_DESCRIPTION_BEGIN
    !insertmacro MUI_DESCRIPTION_TEXT ${SecDummy} $(DESC_SecDummy)
  !insertmacro MUI_FUNCTION_DESCRIPTION_END

;--------------------------------
;Uninstaller Section

Section "Uninstall"

  ;ADD YOUR OWN FILES HERE...

  Delete "$INSTDIR\Uninstall.exe"

  RMDir /r "$INSTDIR"
  RMDir /r "$SMPROGRAMS\${PRODUCT}"

  DeleteRegKey /ifempty HKCU "Software\${PRODUCT}"
  DeleteRegKey HKLM "Software\${PRODUCT}"
  DeleteRegKey HKLM "${ARP}"

SectionEnd